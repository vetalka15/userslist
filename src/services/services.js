import * as axios  from 'axios';

export default class UserService {

  getResource = (activePage,{ name = '', city = '', email = '', funds = '', phone = '' }) => {
    return axios
      .get(`http://localhost:8080/?offset=${(activePage - 1)*10}&limit=10&name=${name}&email=${email}&city=${city}&funds=${funds}&phone=${phone}`)
  };
  putResource1 = ( id, name, city, email, funds, phone ) => {
    return axios.put(`http://localhost:8080/update/${id}`,
      {name, city, email, funds, phone})
  };
};
