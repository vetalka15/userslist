import React, { Component } from 'react';
import './editUserModal.css';
import UserService from '../../services/services';

class EditUserModal extends Component {
  userService = new UserService();

  state = {
    id:  '',
    item: '',
    name: '',
    city: '',
    email: '',
    funds: '',
    phone: ''
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.item !== this.props.item) {
      this.setState({ ...nextProps.item });
    }
  }
  onPutItem = () => {
    const { id, name, city, email, funds, phone } = this.state;
    this.userService.putResource1(id, name, city, email, funds, phone)
      .then(() => {
        this.props.onUpdateItems();
      });

    this.props.onCloseModal();
  };


  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  render(){
    const { name, city, email, funds, phone } = this.state;

    if (!this.props.openModal) return null;

    return (
    <div className="editModal" >
    <div className="editModal-wrp" >
      <p>Name</p>
      <input
        name="name"
        type="text"
        value={name}
        onChange={this.handleChange}
      />
      <p>Email</p>
      <input
        name="email"
        type="text"
        value={email}
        onChange={this.handleChange}
      />
      <p>Funds</p>
      <input
        name="funds"
        type="text"
        value={funds}
        onChange={this.handleChange}
      />
      <p>City</p>
      <input
        name="city"
        type="text"
        value={city}
        onChange={this.handleChange}
      />
      <p>Phone</p>
      <input
        name="phone"
        type="text"
        value={phone}
        onChange={this.handleChange}
      />
      <div className="editModal-btns" >
         <button onClick={this.onPutItem}>Save</button>
         <button onClick={this.props.onCloseModal}>Cancel</button>
      </div>
    </div>
    </div>
  )
}
}

export default EditUserModal;
