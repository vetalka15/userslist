import React, { Component } from 'react';
import './searchForm.css';



class SearchForm extends Component {

    state = {
      name: '',
      email: '',
      funds: '',
      city: '',
      phone: ''
    };

    onSearchChange =(event) => {
      this.setState({ [event.target.name]: event.target.value })
    };

    render(){
      const { onFilter } = this.props;
      const { name, email, funds, city, phone} = this.state;
        return (
          <div
            className="search-form"
          >
            <p className="search-form-title">Filter</p>
            <div className="search-form-inputs">
              <div>
                Name
                <input
                  type="text"
                  name="name"
                  value={name}
                  onChange={this.onSearchChange}
                />
              </div>
              <div>
                Email
                <input
                  type="text"
                  name="email"
                  value={email}
                  onChange={this.onSearchChange}
                />
              </div>
              <div>
                Funds
                <input
                  type="text"
                  name="funds"
                  value={funds}
                  onChange={this.onSearchChange}
                />
              </div>
              <div>
                City
                <input
                  type="text"
                  name="city"
                  value={city}
                  onChange={this.onSearchChange}
                />
              </div>
              <div>
                Phone
                <input
                  type="text"
                  name="phone"
                  value={phone}
                  onChange={this.onSearchChange}
                />
              </div>
              <button
                onClick={() => onFilter(this.state)}
              >
                apply filter
              </button>
            </div>

          </div>
        )
    }
}

export default SearchForm;