import React, { Component } from 'react';
import './table.css';
import EditUserModal from '../EditUserModal';

class Table extends Component {

  state = {
    item: null,
    openModal: false
  };

  onCloseModal = () => {
    this.setState({ openModal: false });
  };

  render(){

    const renderRow = (item) => {
      return (
        <tr key={item.id}>
          <td style={{width: '60px'}}>
            {item.id}
            </td>
          <td style={{width: '190px'}}>
            {item.name}
          </td>
          <td style={{width: '270px'}}>
            {item.email}
          </td>
          <td style={{width: '70px'}}>
            {item.funds}
          </td>
          <td style={{width: '190px'}}>
            {item.city}
          </td>
          <td style={{width: '220px'}}>
            {item.phone}
          </td>
          <td style={{width: '70px', textAlign: 'center'}}>
            <button
              onClick={() => this.setState({ item, openModal: true })}
            >
              Edit
            </button>
          </td>
        </tr>
      );
    };

      const { items } = this.props;

      return (
        <div >
          <table className="table">
            <thead>
            <tr>
              <th>#</th>
              <th>Name</th>
              <th>Email</th>
              <th>Funds</th>
              <th>City</th>
              <th>Phone</th>
              <th></th>
            </tr>
            </thead>

            <tbody>
            {items.map(renderRow)}
            </tbody>
          </table>

          <EditUserModal
            item={this.state.item}
            onCloseModal={this.onCloseModal}
            openModal={this.state.openModal}
            onUpdateItems={this.props.onUpdateItems}
          />

        </div>

      )
  }
}

export default Table;