import React, { Component } from 'react';
import Table from '../Table';
import SearchForm from '../SearchForm';
import Pagination from "react-js-pagination";
import UserService from '../../services/services';
import './App.css';

class App extends Component {
  userService = new UserService();

  state = {
    items: null,
    totalItems: null,
    activePage: 1,
    searchKeys: ''
  };

  componentDidMount() {
    this.onFetchItems();
  };

  componentDidUpdate(prevProps, prevState){
    if (prevState.activePage !== this.state.activePage) {
      this.onFetchItems();
    }
  };

  fetchItems = (activePage, searchKeys) => {
    this.userService.getResource(activePage, searchKeys)
      .then(res => {
        this.setState({ items: res.data.data, totalItems: res.data.count});
      });
  };

  onFetchItems = () => {
    const { activePage, searchKeys } = this.state;
    this.fetchItems(activePage, searchKeys)
  };

  handlePageChange = (pageNumber)  => {
    this.setState({ activePage: pageNumber })
  };

  onSearchSubmit = (keys) => {
    this.setState({ searchKeys: keys },() => {
      this.onFetchItems();
    });
  };

  render(){

    const { activePage, items, totalItems } = this.state;

      return (
        <div>
          <SearchForm
            onFilter={this.onSearchSubmit}
          />
          {
            items &&
          <Table
            items={items}
            onUpdateItems={() => { this.onFetchItems() }}
          />}
          {
            items &&
          <Pagination
            hideDisabled
            activePage={activePage}
            itemsCountPerPage={10}
            totalItemsCount={totalItems}
            pageRangeDisplayed={5}
            onChange={this.handlePageChange}
          />
          }
        </div>

      )
  }
}

export default App;